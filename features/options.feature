Feature: Jekyll-git-authors uses options specified by user

  Background:
    Given There is a fixture named "page.md" and it is in a subdirectory named "subdir"

  Scenario: User wants to have different number of max authors
    Given There is 8 commits on the "subdir/page.md"
    And There is "_config.yml" with "max_authors" set to "7"
    When Jekyll generates authors with set option
    Then There should be 7 authors on the "subdir/page.md"

  Scenario: User sets multiple options at once
    Given There are 2 commits on the "subdir/page.md"
    And There is "_config.yml" with following configuration:
    """
    git_authors:
      center: false
      thematic_break: false
      prefix: 'Contributors:'
    """
    When Jekyll generates authors with set option
    Then Authors should not be centered
    And Authors should not have thematic break
    And Authors should have "Contributors:" prefix

  Scenario Outline: Setting one option
    Given There are 2 commits on the "subdir/page.md"
    And There is "_config.yml" with "<option>" set to "<value>"
    When Jekyll generates authors with set option
    Then Authors should <result>

    Examples:
      | option         | value           | result                      |
      | center         | false           | not be centered             |
      | thematic_break | false           | not have thematic break     |
      | prefix         | 'Contributors:' | have "Contributors:" prefix |
