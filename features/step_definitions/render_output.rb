Given('There is a fixture named {string} and it is in a subdirectory named {string}') do |file, subdir|
  FileUtils.cp "#{@original_dir}/features/fixtures/#{file}", File.join(@dir, subdir)
  expect(File).to exist(File.join(subdir, file))
end

Given('There is a fixture {string}') do |file|
  FileUtils.cp "#{@original_dir}/features/fixtures/page.md", @dir
  expect(File).to exist(file)
end

Given('There is/are {int} commit(s)( from different authors) on the {string}') do |int, file|
  next if int.zero?

  GitHelper.add_commits_different_authors file, int
end

Given('There are {int} commits from the same author on the {string}') do |int, file|
  next if int.zero?

  GitHelper.add_commits_same_authors file, int
end

When('Jekyll-git-authors generates authors') do
  # Jekyll::Site does not load directory properly if
  # defaults are being loaded more than once.
  # Workaround is copying it into variable and override directories.
  site_config = Jekyll::Configuration::DEFAULTS.dup
  site_config['source'] = @dir
  site_config['destination'] = File.join(@dir, '_site')
  # Suppress jekyll log output.
  Jekyll.logger.log_level = :error

  @site = Jekyll::Site.new(site_config)
  authors_generator = Jekyll::AuthorsGenerator.new(@site.config)
  @site.generators = Array(authors_generator)

  # Make jekyll generate every page found and add them into site.pages variable.
  files = Dir.glob File.join('**', '*.md')
  files.each do |file|
    @page = Jekyll::Page.new(@site, File.expand_path(Dir.pwd), '', file)
    @site.pages << @page
  end
  @site.generate
end

Then('There should be {int} author(s) on the {string}') do |int, file|
  page = @site.pages.find { |p| p.path == file }
  expression = /\[[^\]]+\]\(mailto:\{\{ '[^']*' \| encode_email \}\}\)/
  matched_authors = page.content.scan(expression).size

  expect(matched_authors).to eq(int)
end
