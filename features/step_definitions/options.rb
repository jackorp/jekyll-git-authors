Given('There is {string} with {string} set to {string}') do |config_yml, option, value|
  File.open(config_yml, 'w') do |file|
    file.write("git_authors:\n  #{option}: #{value}")
  end
  jekyll_configuration = Jekyll::Configuration.new
  config = jekyll_configuration.read_config_file(config_yml)
  @site_config = Jekyll::Configuration.from config
end

Given('There is {string} with following configuration:') do |config_yml, options|
  File.open('_config.yml', 'w') { |file| file.write(options) }
  jekyll_configuration = Jekyll::Configuration.new
  config = jekyll_configuration.read_config_file(config_yml)
  @site_config = Jekyll::Configuration.from config
end

When('Jekyll generates authors with set option') do
  @site_config['source'] = @dir
  @site_config['destination'] = File.join(@dir, '_site')
  # Suppress jekyll log output.
  Jekyll.logger.log_level = :error

  @site = Jekyll::Site.new(@site_config)
  authors_generator = Jekyll::AuthorsGenerator.new(@site.config)
  @site.generators = Array(authors_generator)

  # Make jekyll generate every page found and add them into site.pages variable.
  files = Dir.glob File.join('**', '*.md')
  files.each do |file|
    @page = Jekyll::Page.new(@site, File.expand_path(Dir.pwd), '', file)
    @site.pages << @page
  end
  @site.generate
end

Then('Authors should not be centered') do
  center = %r|\{:center: style="text-align: center"\}\n[^\n]*\n\{:center\}|
  page = @site.pages.find { |p| p.path == 'subdir/page.md' }
  expect(page.content).to_not match(center)
end

Then('Authors should not have thematic break') do
  thematic_break = %r|\n_  _  _  _\n{: #authors-rule}|
  page = @site.pages.find { |p| p.path == 'subdir/page.md' }
  expect(page.content).to_not match(thematic_break)
end

Then('Authors should have {string} prefix') do |prefix|
  page = @site.pages.find { |p| p.path == 'subdir/page.md' }
  expect(page.content).to match(prefix)
end
