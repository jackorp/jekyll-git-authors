$LOAD_PATH.unshift File.expand_path('../../lib', __dir__)
require 'tmpdir'
require 'fileutils'
require 'jekyll'
require 'jekyll-git-authors'
# Using this file as a global hook to set up environment

if ENV['CODE_COV']
  require 'simplecov'
  SimpleCov.start
end

Before do
  @original_dir = Dir.pwd
  @dir = Dir.mktmpdir 'jekyll-git-authors'
  @subdir = File.join(@dir, 'subdir')

  Dir.mkdir @subdir

  Dir.chdir @dir

  # Initialize git
  GitHelper.init_repo
end

After do
  Dir.chdir @original_dir
  FileUtils.rm_rf @dir
end
