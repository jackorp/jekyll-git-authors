module GitHelper
  # Initialize git environment in current directory.
  # @commit_count is used to track how many times was the author changed.
  def self.init_repo
    JekyllGitAuthors::Git.execute 'init'

    @commit_count = 0
  end

  # Change the author name and email by adding additional_info provided when the function gets called.
  def self.change_author(additional_info)
    JekyllGitAuthors::Git.execute "config user.email 'you#{additional_info}@example.com'"
    JekyllGitAuthors::Git.execute "config user.name 'Your Name #{additional_info}'"
  end

  # Write "Commit #'number'" into the file and then commit the change
  def self.add_commit(file)
    File.open(file, 'a') do |f|
      f << "\nCommit ##{@commit_count}"
    end

    JekyllGitAuthors::Git.execute "add #{file}"
    JekyllGitAuthors::Git.execute %Q|commit -m "Commit ##{@commit_count}."|
  end

  # Add multiple commits from same author.
  #
  # First increment commit_count then n times commit
  def self.add_commits_same_authors(file, number = 1)
    @commit_count += 1
    change_author @commit_count

    number.times do
      add_commit file
    end
  end

  # Commit multiple times with different authors.
  #
  # N times increment commit_count and then commit.
  def self.add_commits_different_authors(file, number = 1)
    number.times do
      @commit_count += 1
      change_author @commit_count

      add_commit file
    end
  end
end
