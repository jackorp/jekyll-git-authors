Feature: Jekyll renders output of jekyll-git-authors

  Background:
    Given There is a fixture named "page.md" and it is in a subdirectory named "subdir"

  Scenario: Authors are generated correctly
    Given There are 6 commits on the "subdir/page.md"
    When Jekyll-git-authors generates authors
    Then There should be 5 authors on the "subdir/page.md"

  Scenario: Authors are not generated for non-section pages
    Given There is a fixture "page.md"
    And There is 1 commit on the "page.md"
    And There is 1 commit on the "subdir/page.md"
    When Jekyll-git-authors generates authors
    Then There should be 0 authors on the "page.md"
    But There should be 1 author on the "subdir/page.md"

  Scenario Outline: Generating authors when there are multiple commits from same author
    Given There is a fixture "subdir/page.md"
    And There are <different> commits from different authors on the "subdir/page.md"
    And There are <same> commits from the same author on the "subdir/page.md"
    When Jekyll-git-authors generates authors
    Then There should be <authors> authors on the "subdir/page.md"

    Examples:
      | same | different | authors |
      |    2 |         1 |       2 |
      |    5 |         5 |       5 |
      |    3 |         2 |       3 |
      |    0 |        10 |       5 |
      |   10 |         0 |       1 |
      |    1 |         1 |       2 |
      |   10 |         1 |       2 |
      |    1 |        10 |       5 |
