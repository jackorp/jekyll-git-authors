[![coverage report](https://gitlab.com/jackorp/jekyll-git-authors/badges/master/coverage.svg)](https://jackorp.gitlab.io/jekyll-git-authors) [![pipeline status](https://gitlab.com/jackorp/jekyll-git-authors/badges/master/pipeline.svg)](https://gitlab.com/jackorp/jekyll-git-authors/commits/master)

# Git Authors Generator for Jekyll

This generator adds last 5 authors and their obfuscated email adress from git commit history into markdown formatted page.
Be sure to also have [jekyll-email-protect](https://github.com/vwochnik/jekyll-email-protect) installed correctly to obfuscate email addresses!

## Installation

This plugin is available as a [rubygem](https://rubygems.org/gems/jekyll-git-authors).

Add this line to your application's `Gemfile`:

```
gem 'jekyll-git-authors'
```

And then execute the `bundle` command to install the gem.

Alternatively, you can also manually install the gem using the following command:

```
$ gem install jekyll-git-authors
```

After the plugin has been installed successfully, add the following lines to your `_config.yml` in order to tell Jekyll to use the plugin:

```
plugins:
- jekyll-git-authors
```
## Contributing

Bug reports and pull requests are welcome at https://gitlab.com/jackorp/jekyll-git-authors.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
