require_relative './test_helper'
require 'jekyll-git-authors'

class TestJekyllGitAuthors < Minitest::Test
  def setup
    @original_dir = Dir.pwd
    @dir = Dir.mktmpdir 'jekyll-git-authors'

    @subdir = File.join(@dir, 'subdir')

    Dir.mkdir @subdir

    FileUtils.cp './test/fixtures/README.md', @subdir

    Dir.chdir @dir

    JekyllGitAuthors::Git.execute 'init'

    JekyllGitAuthors::Git.execute 'config user.email "you@example.com"'
    JekyllGitAuthors::Git.execute 'config user.name "Your Name"'

    JekyllGitAuthors::Git.execute 'add .'
    JekyllGitAuthors::Git.execute 'commit -m "Initial commit"'
  end

  def teardown
    Dir.chdir @original_dir
    FileUtils.rm_rf @dir
  end

  def test_that_authors_method_formats_correctly
    expected = "\n_  _  _  _\n{: #authors-rule}\n{:center: style=\"text-align: center\"}" \
      "\nAuthors: [Your Name](mailto:{{ 'you@example.com' | encode_email }})\n{:center}"

    authors_generator = Jekyll::AuthorsGenerator.new
    actual = authors_generator.authors 'subdir/README.md'

    assert_equal expected, actual
  end

  def test_format
    authors = [JekyllGitAuthors::Author.new('Your Name', 'you@example.com')]
    expected = "\n_  _  _  _\n{: #authors-rule}\n{:center: style=\"text-align: center\"}\n" \
      "Authors: [Your Name](mailto:{{ 'you@example.com' | encode_email }})\n{:center}"

    authors_generator = Jekyll::AuthorsGenerator.new
    actual = authors_generator.format authors

    assert_equal expected, actual
  end
end
