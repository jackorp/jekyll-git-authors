require_relative 'test_helper'

class TestAuthor < Minitest::Test
  def test_spaceship_operator
    authors = []
    3.downto(0) do |num|
      name, email = "you#{num}", "you#{num}@example.com"
      author = JekyllGitAuthors::Author.new name, email
      authors << author
    end

    assert_equal authors.last, authors.min
    assert_equal authors.first, authors.max
  end

  def test_to_markdown
    name, email = 'John Doe', 'john@doe.com'
    author = JekyllGitAuthors::Author.new name, email

    assert_equal "[#{name}](mailto:{{ '#{email}' | encode_email }})", author.to_markdown.to_s
    assert_kind_of JekyllGitAuthors::Markdown, author.to_markdown
  end
end
