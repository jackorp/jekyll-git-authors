require 'fileutils'
require_relative './test_helper'

class TestGit < Minitest::Test
  def setup
    @original_dir = Dir.pwd
    @dir = Dir.mktmpdir 'jekyll-git-authors'

    @subdir = File.join(@dir, 'subdir')

    Dir.mkdir @subdir

    FileUtils.cp './test/fixtures/README.md', @subdir

    Dir.chdir @dir

    JekyllGitAuthors::Git.execute 'init'

    JekyllGitAuthors::Git.execute 'config user.email "you@example.com"'
    JekyllGitAuthors::Git.execute 'config user.name "Your Name"'

    JekyllGitAuthors::Git.execute 'add .'
    JekyllGitAuthors::Git.execute 'commit -m "Initial commit"'
  end

  def teardown
    Dir.chdir @original_dir
    FileUtils.rm_rf @dir
  end

  def test_that_git_executes_basic_commands
    assert_match(/^git version /, JekyllGitAuthors::Git.execute('--version'))
  end

  def test_that_git_raises_execution_error_on_invalid_command
    invalid_command = 'patses'
    err_message = "Git command failed: '#{invalid_command}', "

    exception = assert_raises(JekyllGitAuthors::Git::ExecutionError) do
      JekyllGitAuthors::Git.execute invalid_command
    end
    assert_match err_message, exception.message
  end

  def test_git_log
    method_output = JekyllGitAuthors::Git.log File.join(@subdir, 'README.md')
    assert_equal 'Your Name;you@example.com', method_output.to_s
  end

  def test_that_git_log_raises_exception_with_message_when_file_is_not_tracked_in_git
    FileUtils.cp "#{@original_dir}/test/fixtures/README.md", @dir
    file = './README.md'
    err_message = "Git command failed: 'ls-files --error-unmatch ./README.md'"

    exception = assert_raises(JekyllGitAuthors::Git::ExecutionError) do
      JekyllGitAuthors::Git.log(file)
    end
    assert_match err_message, exception.message
  end

  def test_log_called_when_there_is_no_git_repository
    FileUtils.rm_rf "#{@dir}/.git"
    err_message = "Git command failed: 'ls-files"

    exception = assert_raises(JekyllGitAuthors::Git::ExecutionError) do
      JekyllGitAuthors::Git.log File.join(@subdir, 'README.md')
    end
    assert_match err_message, exception.message
  end
end

class TestGitExecutionError < Minitest::Test
  def test_execution_error_has_redeable_executed_command_stdout_and_stderr
    command, stdout, stderr = 'add .', 'foo', 'bar'

    exception = assert_raises(JekyllGitAuthors::Git::ExecutionError) do
      raise JekyllGitAuthors::Git::ExecutionError.new(command, stdout, stderr)
    end

    assert_equal command, exception.command
    assert_equal stdout, exception.stdout
    assert_equal stderr, exception.stderr
  end

  def test_execution_error_has_default_message
    command = 'ckommnd'
    err_message = "Git command failed: '#{command}', Git error message: "

    exception = assert_raises(JekyllGitAuthors::Git::ExecutionError) do
      raise JekyllGitAuthors::Git::ExecutionError.new command, nil, nil
    end

    assert_match err_message, exception.message
  end
end

class TestGitInvalidEncoding < Minitest::Test
  def test_invalid_encoding_has_default_message
    err_message = 'Git log contains invalid UTF-8 bytes.'

    exception = assert_raises(JekyllGitAuthors::Git::InvalidEncoding) do
      raise JekyllGitAuthors::Git::InvalidEncoding
    end

    assert_match err_message, exception.message
  end
end

class TestGitLog < Minitest::Test
  def author
    JekyllGitAuthors::Author.new('Your Name', 'you@example.com')
  end

  def test_latest_email_is_used
    output = "Your Name;you@example.com\nYour Name;me@example.com"
    git_log = JekyllGitAuthors::Git::Log.new output
    assert_equal [author], git_log.authors
  end

  def test_that_it_logs_single_author_once_if_author_has_multiple_commits
    output = "Your Name;you@example.com\nYour Name;you@example.com"
    git_log = JekyllGitAuthors::Git::Log.new output
    assert_equal [author], git_log.authors
  end

  def test_that_it_logs_multiple_authors
    output = "Your Name;you@example.com\nMy Name;me@example.org"
    git_log = JekyllGitAuthors::Git::Log.new output
    assert_equal [author, JekyllGitAuthors::Author.new('My Name', 'me@example.org')], git_log.authors
  end

  def test_method_authors_stops_when_max_authors_count_is_specified
    authors = ''
    9.times do |num|
      authors << "Your Name#{num};you#{num}@example.com\n"
    end
    git_log = JekyllGitAuthors::Git::Log.new authors, 7

    assert_equal 7, git_log.authors.size
  end

  def test_method_authors_iterates_through_authors
    authors = ''
    3.times do
      authors << "Your Name0;you0@example.com\n"
    end
    3.times do
      authors << "Your Name1;you1@example.com\n"
    end
    2.upto(8) do |num|
      authors << "Your Name#{num};you#{num}@example.com\n"
    end

    git_log = JekyllGitAuthors::Git::Log.new authors

    assert_equal JekyllGitAuthors::Author.new('Your Name4', 'you4@example.com'), git_log.authors.last
    assert_equal 5, git_log.authors.size
  end

  def test_git_log_parses_valid_utf8_bytes_and_outputs_authors
    valid_bytes = "\xC4\x9B\xC5\xA1\xC4\x8D\xC5\x99;foo@bar.com".force_encoding('ASCII-8BIT')
    git_log = JekyllGitAuthors::Git::Log.new valid_bytes

    assert_equal [JekyllGitAuthors::Author.new('ěščř', 'foo@bar.com')], git_log.authors
  end

  def test_that_git_log_raises_custom_exception_with_message_if_log_contains_invalid_utf8_bytes
    # The sequence has randomly erased bytes.
    invalid_bytes = "\xE3\xD3\x8B\xE3\x83\xBC\x83\xE3\x83\xE3\x82\xB3\xA3\x82\x99" \
      "\xE3\x83\xB3\xE3\x83\x8F\xE3\x82\x9A\xC3\xBD;foo@bar.com"
    msg = 'Git log contains invalid UTF-8 bytes.'

    exception = assert_raises(JekyllGitAuthors::Git::InvalidEncoding) do
      JekyllGitAuthors::Git::Log.new invalid_bytes
    end

    assert_match msg, exception.message
  end
end
