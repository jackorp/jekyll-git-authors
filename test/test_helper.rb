$LOAD_PATH.unshift File.join(File.dirname(__FILE__), '..', 'lib')

if ENV['CODE_COV']
  require 'simplecov'
  SimpleCov.start
end

require 'minitest/autorun'
