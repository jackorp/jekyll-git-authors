require_relative './test_helper'
require 'jekyll-git-authors/markdown'

class TestMarkdown < Minitest::Test
  def setup
    @author, @email = 'you', 'you@example.com'
    @author_email = [JekyllGitAuthors::Author.new(@author, @email)]
    @text = 'foobar'
  end

  def markdown(text)
    JekyllGitAuthors::Markdown.new(text)
  end

  def test_markdown_center
    assert_match %Q|{:center: style="text-align: center"}\n#{@text}\n{:center}|, markdown(@text).center.to_s
  end

  def test_markdown_thematic_break
    assert_match "\n_  _  _  _\n{: #authors-rule}\n#{@text}", markdown(@text).thematic_break.to_s
  end

  def test_prefix
    assert_match "Authors: #{@text}", markdown(@text).prefix('Authors:').to_s
  end
end
