require_relative 'markdown'

module JekyllGitAuthors
  Author = Struct.new(:name, :email) do
    def <=>(author)
      name <=> author.name
    end

    def to_markdown
      JekyllGitAuthors::Markdown.new("[#{name}](mailto:{{ '#{email}' | encode_email }})")
    end
  end
end
