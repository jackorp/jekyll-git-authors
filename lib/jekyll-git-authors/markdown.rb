module JekyllGitAuthors
  class Markdown
    # @param [String] text - string that will be edited.
    def initialize(text)
      @text = text
    end

    # Center @text.
    # @return [Markdown]
    def center
      text = %Q|{:center: style="text-align: center"}\n#{@text}\n{:center}|
      self.class.new text
    end

    # Insert horizontal rule with ID 'authors-rule' above '@text'.
    # @return [Markdown]
    def thematic_break
      text = "\n_  _  _  _\n{: #authors-rule}\n#{@text}"
      self.class.new text
    end

    # Insert prefix before '@text'.
    # e.g. +markdown.prefix('Authors:')+ generates 'Authors: foo, bar'
    # @return [Markdown]
    def prefix(prefix)
      text = "#{prefix} #{@text}"
      self.class.new text
    end

    # Return @text.
    # @return [String]
    def to_s
      @text
    end
  end
end
