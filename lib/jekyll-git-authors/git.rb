require 'open3'
require_relative 'author'

module JekyllGitAuthors
  module Git
    # Git::Log parses output passed from Git#log and creates a hash with author as a key and email as value
    class Log
      # We are expecting output from "git log" command
      # max_authors_count - maximum number of unique authors to return
      def initialize(log, max_authors_count = 5)
        @log = log.force_encoding(Encoding::UTF_8)
        @max_authors_count = max_authors_count

        raise InvalidEncoding unless @log.valid_encoding?
      end

      # Create array of unique authors and return maximum specified by @max_authors_count.
      def authors
        @log.each_line
          .map { |author| author.strip.split(';') }
          .map { |name, email| JekyllGitAuthors::Author.new name, email }
          .uniq { |author| author[:name] }
          .take @max_authors_count
      end

      # To ensure log will be text when we want
      def to_s
        @log
      end
    end

    class ExecutionError < StandardError
      attr_reader :command, :stdout, :stderr

      def initialize(command, stdout, stderr)
        @command = command
        @stdout = stdout
        @stderr = stderr

        super "Git command failed: '#{@command}', Git error message: #{@stderr}"
      end
    end

    # Custom exception to raise when Git::Log receives invalid log.
    class InvalidEncoding < StandardError
      def initialize(msg = 'Git log contains invalid UTF-8 bytes.')
        super
      end
    end

    # Take a git command and execute
    # Raise Git::ExecutionError if command didn't execute successfully
    # then return output of the command.
    # Print custom message if required.
    def self.execute(command)
      stdout, stderr, status = Open3.capture3("git #{command}")

      raise Git::ExecutionError.new(command, stdout, stderr) unless status.success?

      stdout
    end

    # Check if file is tracked with git.
    # If it is not, exit status 1 will be returned by git.
    # so it will be aborted by execute method.
    # max_authors_count - maximum of returned authors
    #
    # Create instance of Git::Log and pass it output from git log command
    # return 5 last authors of a particular file
    # %an returns author, semicolon for effortless parsing, %ae return email of author
    def self.log(file, max_authors_count = 5)
      Git.execute("ls-files --error-unmatch #{file}")
      Log.new(execute("log --pretty=format:'%an;%ae' #{file}"), max_authors_count)
    end
  end
end
