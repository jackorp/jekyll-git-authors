require 'jekyll-git-authors/version'
require 'jekyll-git-authors/markdown'
require 'jekyll-git-authors/git'
require 'jekyll-git-authors/author'

require 'jekyll'

module Jekyll
  class AuthorsGenerator < Generator
    safe true
    priority :high

    def initialize(config = {})
      super(config)

      @options = {
        'max_authors' => 5,
        'center' => true,
        'thematic_break' => true,
        'prefix' => 'Authors:'
      }.merge(config['git_authors'] || {})
    end

    # iterate through pages then pick those which are
    # are markdown file then call Git#log on the page
    # and add authors into its content
    def generate(site) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      Jekyll.logger.info 'Git authors:', 'Generating authors...'

      site.pages.each do |page|
        page_path = page.path
        # If file ends with md and if it is subdir.
        # Jekyll does not add "./" into pages paths, so we can use this approach.
        next unless page_path =~ /\.md/ && page_path =~ /\//

        Dir.chdir File.dirname(page_path) do
          begin
            output = authors File.basename(page_path)
          rescue JekyllGitAuthors::Git::ExecutionError => e
            Jekyll.logger.error 'Error:', "Git authors: #{page_path} is not in git, cannot generate authors."
            Jekyll.logger.error 'Git authors failed,', " Command: '#{e.command}', Git: '#{e.stderr}'"
            break
          end

          Jekyll.logger.debug 'Git authors:', "Generating authors for #{page_path}"

          page.content += output
        end
      end
    end

    # Gets the authors and then adds required formatting.
    def authors(file)
      author_md = JekyllGitAuthors::Git.log(file, @options['max_authors']).authors
      author_md = author_md.sort
      format author_md
    end

    def format(authors)
      markdown = JekyllGitAuthors::Markdown.new authors.map(&:to_markdown).join(', ')
      markdown = markdown.prefix(@options['prefix'])
      markdown = markdown.center if @options['center']
      markdown = markdown.thematic_break if @options['thematic_break']
      markdown.to_s
    end
  end
end
