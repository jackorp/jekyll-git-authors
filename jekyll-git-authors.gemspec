lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll-git-authors/version'

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-git-authors'
  spec.version       = JekyllGitAuthors::VERSION
  spec.authors       = ['Jaroslav Prokop']
  spec.email         = ['jar.prokop@volny.cz']

  spec.summary       = 'Jekyll plugin that adds git authors into your page.'
  spec.description   = 'Jekyll plugin that adds git authors and their obfuscated email address' \
    'into page using markdown and liquid.'
  spec.homepage      = 'https://gitlab.com/jackorp/jekyll-git-authors'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.test_files = `git ls-files -- {test,spec,features}/*`.split("\n")
  spec.require_paths = ['lib']

  spec.add_dependency 'jekyll'
  spec.add_dependency 'jekyll-email-protect'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'cucumber'
  spec.add_development_dependency 'minitest'
  spec.add_development_dependency 'rake', '~> 12'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
end
